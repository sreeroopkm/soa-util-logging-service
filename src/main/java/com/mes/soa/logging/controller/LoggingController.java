package com.mes.soa.logging.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mes.soa.logging.model.RequestPayload;
import com.mes.soa.logging.service.LoggingService;




@RestController
public class LoggingController {
	
	@Autowired
	LoggingService logging;
	
	@SuppressWarnings("rawtypes")
	@PutMapping("/v1/utility/logging")
	@ResponseBody
	public ResponseEntity logController(@RequestBody RequestPayload requestBody)  {
		
		logging.doLog(requestBody);
		return new ResponseEntity(HttpStatus.NO_CONTENT);
		
	}
}
