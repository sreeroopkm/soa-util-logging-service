package com.mes.soa.logging.service;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.mes.soa.logging.model.RequestPayload;


@Service
public class LoggingServiceImpl implements LoggingService {
	  Logger LOGGER = LogManager.getLogger(LoggingServiceImpl.class);
	      
		@Override
		public boolean doLog(RequestPayload requestBody) {
			if(requestBody!=null) {
						Level level = Level.INFO;
						if(requestBody.getLoglevel().equalsIgnoreCase("DEBUG")) {
							level = Level.DEBUG;
						}
						else if(requestBody.getLoglevel().equalsIgnoreCase("WARN")) {
							level = Level.WARN;
						}
						else if(requestBody.getLoglevel().equalsIgnoreCase("ERROR")) {
							level = Level.ERROR;
						}
						else if(requestBody.getLoglevel().equalsIgnoreCase("FATAL")) {
							level = Level.FATAL;
						}
						else if(requestBody.getLoglevel().equalsIgnoreCase("TRACE")) {
							level = Level.TRACE;
						}
						return true;
			}else {
				return false;
			}				
		}

	
}