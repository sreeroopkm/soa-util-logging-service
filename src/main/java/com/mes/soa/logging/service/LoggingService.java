package com.mes.soa.logging.service;

import com.mes.soa.logging.model.RequestPayload;

public interface LoggingService {

	boolean doLog(RequestPayload requestBody) ;
}
