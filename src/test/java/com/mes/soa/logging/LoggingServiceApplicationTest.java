package com.mes.soa.logging;


import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.mes.soa.logging.model.RequestPayload;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

@SpringBootTest
@RunWith(SpringRunner.class)
public class LoggingServiceApplicationTest {

	@Test	
	public void testLogController_200() {
		RequestPayload pathParams = new RequestPayload();
		pathParams.setSystemId("1258");
		pathParams.setLogMessage("MES Logger");
		//pathParams.setTimestamp("1:25");
		
		RequestSpecification request = RestAssured.given();
	    request.body(pathParams);
	    Response resp = request.post("http://localhost:8080/v1/utility/logging");
	    
	    
		int code = resp.getStatusCode();
		System.out.println("Status Code : "+code);
		Assert.assertEquals(code, 200);
	}

	@Test	
	public void testLogController_404() {
		Response resp = RestAssured.post("http://localhost:8080/v1/utility/logging/test");
		int code = resp.getStatusCode();
		System.out.println("Status Code : "+code);
		Assert.assertEquals(code, 404);
	}
}
