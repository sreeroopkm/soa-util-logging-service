FROM openjdk:11
VOLUME /tmp
ADD target/LoggingService-0.0.1.war app.jar
EXPOSE 8080
RUN sh -c 'touch /app.jar'
CMD ["java", "-jar", "-Dspring.profiles.active=default", "/app.jar"]
